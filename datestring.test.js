const datestring = require('./datestring');
let moment = require('moment');
// const unixtime = jest.genMockFromModule('unixtime');
jest.mock('./unixtime');

describe('datestring test', () => {
  it('ms parms set to seconds', () => {
    expect(datestring('YYYY MM DD')).toBe('2019 01 01');
  });
  it('ms parms set to seconds', () => {
    expect(datestring('DD MM YYYY')).toBe('01 01 2019');
  });
  it('error - bad input', () => {
    expect(() => {
      datestring({ 12 : 3});
    }).toThrow();
  });
});