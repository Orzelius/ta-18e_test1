/**
 * returns sum of two numbers
 *
 * @param {string} input
 * @returns {string}
 */
function lowercase (input) {
  if (!(typeof input === 'string' || input instanceof String)){
    throw new Error('bad input');
  }
  return input.toLowerCase();
}
module.exports = lowercase;