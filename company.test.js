const company = require('./company');

it('Create new compay 3 and expect it to mach itself each time', () => {
  const company1 = company(3);
  expect(company).toMatchSnapshot();
});

it('Create new compay 85 and expect it to mach itself each time', () => {
  const company2 = company(85);
  expect(company).toMatchSnapshot();
});

test('Create Company with invalid ID', () => {
  expect(() => {
    company('saas');
  }).toThrow();
});