const toNumber = require('./toNumber');

describe('toNumber', () => {
  test('1 => 1', () => {
    expect(toNumber(1)).toBe(1);
  });

  test('"1" => 1', () => {
    expect(toNumber('1')).toBe(1);
  });

  test('"a" is error', () => {
    expect(() => {
      toNumber('a');
    }).toThrow('value \'a\' is not a number!');
  });

  test('4 => 4', () => {
    expect(toNumber(4)).toBe(4);
  });

  test('"a" is error', () => {
    expect(() => {
      const mockCallback = jest.fn(x => 42 + x);
      toNumber(mockCallback);
    }).toThrow();
  });
});
